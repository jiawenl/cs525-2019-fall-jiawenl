(* ****** ****** *)

(*
//
// Here is a C program
//
// http://rosettacode.org/wiki/Maximum_triangle_path_sum#C
//
*)
#include
"share/atspre_define.hats" // defines some names
#include
"share/atspre_staload.hats" // for targeting C
#include
"share/HATS/atspre_staload_libats_ML.hats" // for ...
#include
"share/HATS/atslib_staload_libats_libc.hats" // for libc
//
(* ****** ****** *)
#include "./../assign00.dats"

staload "libats/libc/SATS/math.sats"
staload "libats/libc/SATS/stdio.sats"


(* ****** ****** *)
//
// HX: 15 points
//
(* ****** ****** *)
//
extern
fun
Maximum_triangle_path_sum(): int
//
(* ****** ****** *)
val tr = g0ofg1(
	$list{int}(
	55,
	94, 48,
	95, 30, 96,
	77, 71, 26, 67,
	97, 13, 76, 38, 45,
	7, 36, 79, 16, 37, 68,
	48, 7, 9, 18, 70, 26, 6,
	18, 72, 79, 46, 59, 79, 29, 90,
	20, 76, 87, 11, 32, 7, 7, 49, 18,
	27, 83, 58, 35, 71, 11, 25, 57, 29, 85,
	14, 64, 36, 96, 27, 11, 58, 56, 92, 18, 55,
	2, 90, 3, 60, 48, 49, 41, 46, 33, 36, 47, 23,
	92, 50, 48, 2, 36, 59, 42, 79, 72, 20, 82, 77, 42,
	56, 78, 38, 80, 39, 75, 2, 71, 66, 66, 1, 3, 55, 72,
	44, 25, 67, 84, 71, 67, 11, 61, 40, 57, 58, 89, 40, 56, 36,
	85, 32, 25, 85, 57, 48, 84, 35, 47, 62, 17, 1, 1, 99, 89, 52,
	6, 71, 28, 75, 94, 48, 37, 10, 23, 51, 6, 48, 53, 18, 74, 98, 15,
	27, 2, 92, 23, 8, 71, 76, 84, 15, 52, 92, 63, 81, 10, 44, 10, 69, 93
	)
)


datatype
mylist =
| mylist_nil of ()
| mylist_cons of (int, mylist)
//
#define nil mylist_nil
#define :: mylist_cons

(* ****** ****** *)

fun get_mylist_length(list: mylist, length: int): int =
  case+ list of
  | mylist_nil () => length
  | mylist_cons(x, xs) => get_mylist_length(xs, length+1)
(* ****** ****** *)

(* ****** ****** *)

fun to_mylist(l:list0(int) ): mylist =
case+ l of
| nil0 () => mylist_nil ()
| cons0(x, xs) => mylist_cons(x, to_mylist(xs))
(* ****** ****** *)



(* ****** The function to get the ith element in the function ****** *)
fun get(l: mylist, ith: int): int =
let 
	fun loop( l: mylist, i: int) =
		case+ l of
		| mylist_nil () => 0
		| mylist_cons(x, xs) =>
			if i = 0 
			then x
			else loop(xs, i - 1)
in
	loop(l, ith)
end
(* ****** End of get ****** *)



(* ****** The function for modify the ith element in the list ****** *)
fun modify(l: mylist, ith: int, v: int) =
let 
	fun loop( l: mylist, i: int, v : int) =
		case+ l of
		| mylist_nil () => mylist_nil ()
		| mylist_cons(x, xs) =>
			if i = 0 
				then mylist_cons(v, xs)
				else mylist_cons(x, loop(xs, i - 1, v))
in
	loop(l, ith, v)
end
(* ****** End of modify ****** *)


(* ****** Max ****** *)
fun max(a: int, b: int): int =
if a > b then a else b
(* ****** End of Max ****** *)

fun sqrt(a: int): int =
let 
fun loop(a: int, b: int) =
if b*b > a
then b - 1
else loop(a, b + 1)
in
loop(a, 1)
end

implement
(* ****** Maximum_triangle_path_sum ****** *)
Maximum_triangle_path_sum() =
let
	val tr = to_mylist(tr)
	val len = get_mylist_length(tr, 0)
	val base = sqrt(8 *len + 1) - 1
	val base = base / 2
	val step = base - 1
	fun loop(l: mylist, i: int, step: int, count: int ) = 
		if i < 0
			then get(l, 0)
			else 
			let
				val m = max(get(l, (i + step)), get(l, (i + step + 1)))
				val pre = get(l, i)
				val curr = modify(l, i, (m + pre))
				val count = count + 1
			in 
				if count = step
				then loop(curr, i - 1, step - 1, 0)
				else loop(curr, i - 1, step, count)
			end
in
loop(tr, len - base - 1, step, 0)
end
(* end of [Maximum_triangle_path_sum.dats] *)


implement
main0(): void =
{
val () =
println! ("max path sum = ", Maximum_triangle_path_sum())
}

