(* ****** ****** *)
//
// How to test
// ./assign00_sol_dats
//
// How to compile:
// myatscc assign00_sol.dats
//
(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#include "./../assign00.dats"

(* ****** ****** *)

implement
factorial(n) =
if n > 0
then n * factorial(n-1) else 1

(* ****** int_test****** *)

implement
int_test() = 
let
fun loop(n:int, c: int) : int =
if c > 0
then 
loop(n+1, c *2)
else
n+1
in
loop(0, 1)
end

(* ****** gheep 0:1,  1: 2,   2: 2*2 , 3: 3*2*2, 4: 4*g(3)*g(2)  ****** *)

implement
gheep(n)=
let
fun loop (n: int, a: int, b: int, i : int ): int =
if n = 0
then a
else
if n =1 
then b
else
loop(n - 1, b, i * b * a, i + 1)
in 
loop(n, 1, 2, 2)
end




(* ******  list_append ****** *)
implement
intlist_append(l1,l2) =
 case+ l1 of
  | intlist_nil() => l2
  | intlist_cons(i, its) => intlist_cons (i,  intlist_append(its, l2) )

fun get_length(l: intlist): int=
case+ l of
| intlist_nil() => 0
| intlist_cons(x, xs) => 1 + get_length(xs)

implement
main0() =
{
val () =
println!("factorial(10) = ", factorial(10))

val () =
println!("int_test() = ",int_test())

val () = 
println! ("gheep(3) =", gheep(3))

val () =
println! ("ghaap(3) =", ghaap(3))

val () = 
println! ("gheep(4) =", gheep(4))

val () =
println! ("ghaap(4) =", ghaap(4))

val () = 
println! ("gheep(5) =", gheep(5))

val () =
println! ("ghaap(5) =", ghaap(5))


val () = 
println!("appended length = ", 
		get_length(
			intlist_append(intlist_cons(2, intlist_cons(3, intlist_nil())),
				intlist_cons(1, intlist_cons(2, intlist_cons(3, intlist_nil())))
				)
		)
		)

}

