%{^
#include <math.h>
%}

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

implement
main0() =
let
  val sqrt2 = $extfcall(double, "sqrt", 2.0)
in
  println!("square root of 2 equals ", sqrt2)
end