(* ****** ****** *)

#staload "./mylib.sats"

(* ****** ****** *)

#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)

(* ****** ****** *)
//
implement
{a}(*tmp*)
print_mylist(xs) =
fprint_mylist(stdout_ref, xs)
implement
{a}(*tmp*)
prerr_mylist(xs) =
fprint_mylist(stderr_ref, xs)
//
implement
{a}(*tmp*)
fprint_mylist
  (out, xs) =
( loop(xs, 0) ) where
{
fun
loop
(xs: mylist(a), i: int): void =
(
case+ xs of
| mylist_nil() => ()
| mylist_cons(x0, xs) =>
  (
  if
  i > 0
  then
  fprint_mylist$sep<>(out);
  fprint_val<a>(out, x0); loop(xs, i+1)
  )
)
}
//
implement{}
fprint_mylist$sep(out) = fprint_string(out, ", ")
//
(* ****** ****** *)
implement
{a}
mylist_length
  (xs) =
( loop(xs, 0) ) where
{
//
fun
loop
(xs: mylist(a), n: int) =
case+ xs of
| mylist_nil() => n
| mylist_cons(_, xs) => loop(xs, n+1)
//
} (* end of [mylist_length] *)

(* ****** ****** *)
implement
{a}
mylist_get_at
  (xs, n) =
( loop(xs, 0) ) where
{
//
fun
loop
(xs: mylist(a), i: int) =
case+ xs of
| mylist_nil() =>
  $raise ListSubscriptExn()
| mylist_cons(x0, xs) =>
  if i >= n then x0 else loop(xs, i+1)
//
} (* end of [mylist_get_at] *)

(* ****** ****** *)

(* end of [mylib.dats] *)
