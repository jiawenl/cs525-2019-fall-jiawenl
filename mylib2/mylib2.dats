#staload "./../mylib/mylib.sats"

#staload "./../mylib/mylib.dats"

#staload "./mylib2.sats"

(* ****** ****** *)

#include
"share/atspre_staload.hats"
(*
#include
"share/atspre_staload_libats_ML.hats"
*)

implement 
fun mylist_contain (l, i) = 
  case+ l of
  | mylist_nil () -> false
  | mylist_cons(x, xs) ->
    if x = i 
    then true
    else contain(xs, i) 