(* ****** ****** *)

#include
"share/atspre_staload.hats"
#include
"share/atspre_staload_libats_ML.hats"

(* ****** ****** *)

#staload "./../mylib/mylib.sats"

(* ****** ****** *)

typedef vnam = string

(* ****** ****** *)

datatype term = 
  | TMint of int
  | TMvar of vnam
  | TMlam of (vnam, term)
  | TMapp of (term, term)
  | TMifz of (term, term, term)

(* ****** ****** *)

fun
term_size
(tm0: term): int =
(
case+ tm0 of
| TMint _ => 1
| TMvar _ => 1
| TMlam (_, tm1) =>
  1 + term_size(tm1)
| TMapp (tm1, tm2) =>
  1 + term_size(tm1) + term_size(tm2)
| TMifz (tm1, tm2, tm3) =>
  1 + term_size(tm1) + term_size(tm2) + term_size(tm3)
)

(* ****** ****** *)

extern
fun
print_term(term): void // stdout
and
prerr_term(term): void // stderr
and
fprint_term(FILEref, term): void

overload print with print_term
overload prerr with prerr_term
overload fprint with fprint_term

(* ****** ****** *)

implement
print_term(tm) =
fprint_term(stdout_ref, tm)
implement
prerr_term(tm) =
fprint_term(stderr_ref, tm)

implement
fprint_term(out, tm0) =
(
case+ tm0 of
| TMint(i) =>
  fprint!(out, "TMint(", i, ")")
| TMvar(x) =>
  fprint!(out, "TMvar(", x, ")")
| TMlam (x, tm1) =>
  fprint!(out, "TMlam(", x, "; ", tm1, ")")
| TMapp (tm1, tm2) =>
  fprint!(out, "TMapp(", tm1, "; ", tm2, ")")
| TMifz (tm1, tm2, tm3) =>
  fprint!(out, "TMifz(", tm1, "; ", tm2, "; ", tm3, ")")
)

(* ****** ****** *)

val K =
let
  val x = TMvar("x")
in
  TMlam("x", TMlam("y", x))
end

(* ****** ****** *)

val
omega =
(
TMlam("x", TMapp(x, x))
) where
{
  val x = TMvar("x")
}

val
Omega = TMapp(omega, omega)

(* ****** ****** *)
//
fun
fomega(f: term): term =
let
  val x = TMvar("x")
in
TMlam
("x", TMapp(f, TMapp(x, x)))
end
//
fun
fOmega(f: term): term =
(
  TMapp(fomega(f), fomega(f))
)
//
(* ****** ****** *)

val () = println!("K = ", K)
val () = println!("omega = ", omega)
val () = println!("Omega = ", Omega)

(* ****** ****** *)

extern
fun
subst0
(tm0: term, x0: vnam, sub: term): term

implement
subst0
(tm0, x0, sub) =
let
fun
helper(tm: term): term = subst0(tm, x0, sub)
in
case+ tm0 of
| TMint _ => tm0
| TMvar(x1) =>
  if
  x0 = x1 then sub else tm0
| TMlam(x1, tm1) =>
  if
  x0 = x1
  then tm0
  else TMlam(x1, helper(tm1))
| TMapp(tm1, tm2) =>
  TMapp(helper(tm1), helper(tm2))
| TMifz(tm1, tm2, tm3) =>
  TMifz(helper(tm1), helper(tm2), helper(tm3))
end

(* ****** ****** *)

extern
fun
interp1 : term -> term // call-by-name
extern
fun
interp2 : term -> term // call-by-value

(* ****** ****** *)

implement
interp1(tm0) =
(
case+ tm0 of
| TMint _ => tm0
| TMvar _ => tm0
| TMlam(x0, tm1) => tm0
| TMapp(tm1, tm2) =>
  let
    val tm1 = interp1(tm1)
  in
    case+ tm1 of
    | TMlam(x1, tm11) =>
      interp1(subst0(tm11, x1, tm2))
    | _(* non-TMlam *) => tm0
  end
| TMifz(tm1, tm2, tm3) =>
  let
  val tm1 = interp1(tm1)
  in
    case- tm1 of
    | TMint(i) =>
      if i = 0 then interp1(tm1) else interp2(tm2)
  end
)

(* ****** ****** *)

implement
interp2(tm0) =
(
case+ tm0 of
| TMint _ => tm0
| TMvar _ => tm0
| TMlam(x0, tm1) => tm0
| TMapp(tm1, tm2) =>
  let
    val tm1 = interp2(tm1)
    val tm2 = interp2(tm2)
  in
    case+ tm1 of
    | TMlam(x1, tm11) =>
      interp2(subst0(tm11, x1, tm2))
    | _(* non-TMlam *) => tm0
  end
| TMifz(tm1, tm2, tm3) =>
  let
  val tm1 = interp1(tm1)
  in
    case- tm1 of
    | TMint(i) =>
      if i = 0 then interp1(tm1) else interp2(tm2)
  end
)

(* ****** ****** *)

val _ = interp1(TMvar("x"))
val _ = interp2(TMvar("x"))

(* ****** ****** *)

implement main0() =
{
  val () = println!("Hello from [main0]!")
}

(* ****** ****** *)

(* end of [lambda0.dats] *)
