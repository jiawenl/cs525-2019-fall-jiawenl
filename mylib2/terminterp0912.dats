
#include "./../mylib/mylib.dats"

typedef vanme = string

typedef term = 
|TMvar of vname
|TMlam of (vname, term)
|TMapp of (term, term)

fun term_size(tm0:term): int =
()


fun
print_term(term):void
and
prerr_term(term): void
and 
fprint_term(stderr_ref, term):void

overload fprint with fprint_term

implement
print_term(tm) =
fprint_term(stdout_ref,  tm)

implement
prerr_term(tm) 
fprint_term(stdout_ref,  tm)

implement
fprint_term(out, tm0) = 
case+ tm0 of
| TMvar(x) =>
| TMlam(x, tm1)
| TMapp


val k =
let val x = TMvar("x")
in
TMlam("x", TMlam("y", x))
end

val omega = 
(TMlam("x", TMapp(x,x))
	)
where
{
	val x = TMvar("x")
}


val 
Omega = TMapp(omega, omega)



val () = println!("K =", k)
val () = println!("omega = ", omega)
val () = println!("Omega = ", Omega)

(******************************************)

fun
subst0 
(tm0: term, x0: vname, sub: term): term

implement

subst0 
(tm0, x0, sub) =
(
	case+ tm0 of
	| TMvar(x1) 		=>
		if x0 = x1 then sub else tm0
	| TMlam(x1, tm1) 	=>
		if x0 = x1 then tm0 else TMlam(x1, subst0(tm1, x0, sub))
	| TMapp(tm1, tm2) 	=>
		TMapp(subst0(tm1, x0, sub), subst0(tm2, x0, sub))
)

(******************************************)

extern
fun interp: term -> term


(******************************************)

implement
interp1(tm0) =
(
	case+ tm0 of
	| TMvar _ => tm0
	| TMlam(x0, tm1) => tm0
//	TMlam(x0, interp(tm1))
	| TMapp(tm1, tm2) =>
	let
	val tm1 = interp1(tm1)
	in
	(
		case+ tm1 of
		|TMlam(x1, tm11) =>
		interp1(subst0(tm11, x1, t2))
		| _ => tm0
		)
)




implement
interp2(tm0) =
(
	case+ tm0 of
	| TMvar _ => tm0
	| TMlam(x0, tm1) => tm0
//	TMlam(x0, interp(tm1))
	| TMapp(tm1, tm2) =>
	let
	val tm1 = interp2(tm1)
	val tm2 = interp2(tm2)
	in
	(
		case+ tm1 of
		|TMlam(x1, tm11) =>
		interp2(subst0(tm11, x1, t2))
		| _ => tm0
		)
)





